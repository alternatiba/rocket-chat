---
title: Mise en forme des messages sur Rocket.Chat
author: Alternatiba
date: Août 2019
theme: beige
---

# Nouvelle version !

Ce tuto n'est plus maintenu.

Rendez-vous sur [la nouvelle version](https://tuto.alternatiba.eu/rocket-chat/mise-en-forme/) !
---

# Sommaire

----

* Introduction
* Les outils de mise en forme
    * Gras, italique
    * Insertion de liens
    * Gestion des paragraphes
    * Listes à puce
    * Citation
    * Titres
* Nous contacter
    * Aide et questions
    * Avis sur ce tuto

---

# Introduction

----

Vous trouverez dans ce tuto les outils nécessaires pour mettre en forme vos messages : texte en gras ou en italique, insertion de liens ou de citations, rédaction de listes à puces, tout cela peut être utilisé simplement dans vos messages Rocket.Chat.

----

La mise en forme s'effectue en insérant des symboles dans le texte (`#`,`*`,`>`,etc.). Une fois le message rédigé, Rocket.Chat les transforme en indications de mise en forme.

----

Pour reproduire les différentes astuces regardez dans les cadres grisés quelles sont les combinaisons de symboles qui ont permis de mettre en forme le texte. Vous devez reproduire **exactement** ces combinaisons : ne mettez pas d'espaces s'il n'y en a pas dans l'exemple, sautez une ligne s'il le faut, etc. _**Bonne lecture**_ !

----

NB : Si vous cherchez des informations qui concernent un autre sujet que la mise en forme des messages, [ce tutoriel de prise en main de Rocket.Chat](https://alternatiba.frama.io/rocket-chat/rocket-chat-tuto.html#/title-slide) est à votre disposition !

---

# Outils de base

----

## Gras, italique, barré

On peut formater le texte avec *de l’italique*, **du gras**, ou ~~du barré~~.

```
*de l’italique*, **du gras**, ou ~~du barré~~.
```

----

## Insertion de liens vers des site web

Vous pouvez ajouter [des hyperliens](https://alternatiba.eu/) au lieu de copier-coller l’url au milieu de votre texte.

```
Ajoutez [des hyperliens](https://alternatiba.eu/)
```

Un aperçu des page web est automatiquement généré en bas de votre message une fois celui-ci publié.

Attention à **ne pas mettre d'espaces** entre les [] et les () ni avant et après le lien hypertexte. Sinon ça ne fonctionne pas !

----

## Gestion des paragraphes

Pour aller à la ligne, appuyez sur `MAJ+Entrée`.

**Attention** : Appuyer sur `Entrée` envoie votre message.

Vous avez envoyé un message incomplet par erreur ? 
Modifiez le message publié : cliquez sur les trois points en haut à droite du message publié, puis sur « Modifier ». 

----

## Utiliser des listes à puces

- faire les courses
- passer l’aspirateur

```
- faire les courses
- passer l’aspirateur
```

Laissez une ligne vide **avant** et **après** votre liste pour que le paragraphe suivant soit bien aligné à gauche

----

## Citation

L'outil citation permet de recopier des mails, des extraits d’articles de presse ou de textes divers, en les distinguant bien du reste du texte.

Comme le disait le poète :

> Lorem ipsum dolor sit amet, consectetur adipiscing elit.

```
> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
```

Laissez une ligne vide **avant** et **après** la citation.

----

## Titres

Vous pouvez ajouter des titres plus ou moins gros :

![](media/16-rocket-chat-titres.png)

```
# Mon titre 1
## Mon titre 2
### Mon titre 3
#### Mon titre 4
##### Mon titre 5
```

---

# Nous contacter

----

## Aide

- Vous avez suivi le tutoriel mais ça ne fonctionne pas ?
- Vous n'avez pas trouvé l'info que vous cherchiez ?

Envoyez un message sur le fil #aide_rocketchat !

----

## Améliorations

Vous souhaitez donner votre avis sur ce tutoriel ? Cela nous intéresse ! Ecrivez-nous vos remarques sur le fil #aide_rocketchat

Merci d'avance pour votre aide !
