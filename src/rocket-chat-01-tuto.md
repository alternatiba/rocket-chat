---
title: Prise en main de Rocket Chat
author: Alternatiba/ANV-COP21
date: Novembre 2019
theme: beige
---

# Nouvelle version !

Ce tuto n'est plus maintenu.

Rendez-vous sur [la nouvelle version](https://tuto.alternatiba.eu/rocket-chat/premiers-pas/) !

---

# Sommaire

Pour utiliser ce tuto, allez vers le bas pour lire un chapitre, puis aller sur la droite pour passer au chapitre suivant.
Bonne lecture :)

----

* Présentation
    * Un outil de chat, pour quoi faire ?
    * Pourquoi Rocket.Chat ?
* Premiers pas
    * Installer Rocket.Chat
    * S'inscrire
    * Découvrir les fils publics principaux
    * Rejoindre un fil de discussion
    * Prendre connaissance de la charte
    * Renseigner sa clef de chiffrement E2E

...

----

* Gestion des fils
    * Créer un fil
    * Modifier un fil
    * Modérer un fil
    * Créer une discussion ou un fil ?
* Utilisation régulière
    * Mise en forme des messages
    * Mentionner des personnes
    * Afficher la liste des membres d'un fil
    * Entamer une conversation privée
    * Répondre dans un thread
    * Utiliser l'outil recherche

...

----

* Gestion des préférences
    * Accéder aux préférences
    * Éditer le nom affiché
    * Gérer les notifications
* Nous contacter
    * Aide et questions
    * Avis sur ce tuto

---

# Présentation

----

## Un outil de chat, pour quoi faire ?

----

* Suivre les actualités du réseau
* Créer du lien entre les groupes et avec l'équipe nationale
* Participer aux groupes de travail nationaux
* Échanger sur des sujets divers avec le réseau
* Regrouper dans un seul outil toutes ces communications

----

## Pourquoi Rocket Chat ?

----

### Logiciel libre & hébergement alternatif

* Rocket.Chat est un logiciel libre. 
* Nous sommes hébergé-es par IndieHosters, association basée à Lyon et membre du collectif [CHATONS](https://chatons.org/) (Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires)

*En clair, cela signifie :*

* Respect de notre vie privée
* Conforme aux valeurs d'Alternatiba et ANV-COP21
* Promotion des alternatives du numérique

----

### Fonctionnalités

* Clarté de l'interface, des systèmes de réponse et de réaction par émoticônes
* Applications mobiles pour Android et Iphone
* Partage de fichiers et d'images
* Notifications par mail hors connexion
* Possibilité de connexion avec d'autres applications pour envoyer ou recevoir des informations (Webhooks, Zapier, …)	

---

# Premiers pas

----

## Installer Rocket.Chat

----

Pour l'**ordinateur** : l'application desktop
* Allez sur [https://rocket.chat/install/](https://rocket.chat/install/) et choisissez le fichier qui correspond à votre ordinateur (linux, mac, windows)
* Si vous avez un Windows, cliquez sur la ligne qui termine par .exe pour déclancher le téléchargement. (rocketchat-setup-3.1.1.exe par exemple)

Pour le **téléphone** : l'application téléphone ou tablette
* Sur le playstore pour les Androids
* Sur l'appstore pour les IPhones

----

Vous pouvez aussi utiliser Rocket.chat sans l'installer en tapant simplement le nom du chat dans votre barre de recherche. *Vous n'avez pas le lien pour accéder au chat ? Demandez-le à la personne qui vous a conseillé de vous inscrire sur le chat ou à admin_rc [arobase] alternatiba.eu !*

----

## S'inscrire

----

* Ouvrez votre navigateur web
* Allez sur le chat d'Alternatiba et ANV-COP21. *Vous n'avez pas le lien pour accéder au chat ? Demandez-le à la personne qui vous a conseillé de vous inscrire sur le chat ou à admin_rc [arobase] alternatiba.eu !*

* Cliquez sur "Créer un nouveau compte"

**Attention** : un bug récurrent sur l'appli Android bloque l'inscription. 
Suivez la démarche conseillée ci-dessus en passant par un navigateur web !

----

Entrez vos informations *prénom (groupe local)* dans la case "Nom" et validez

![](media/02-rocket-chat-inscription.png)

----

Choisissez un pseudo

![](media/03-rocket-chat-choix-pseudo.png)

----

Et bienvenue :)

![](media/04-rocket-chat-accueil.png)

----

## Découvrir les fils publics principaux

----

* Par défaut vous êtes inscrit-es à cinq fils :
    * `#general (infos importantes)`
    * `#le_coin_militant`
    * `#Propagation reseaux sociaux`
    * `#questions_et_annonces`
    * `#le_coin_detente`
* Vous y accédez en cliquant sur leur nom
* Les nouveaux messages vous sont notifiés
* Nous vous conseillons de ne pas vous désabonner de ces fils ;)

----

### Le fil #general

Ce fil sert à diffuser les informations importantes : vie du mouvement, campagnes nationales, fonctionnement de Rocket Chat (nouveautés, mises à jour).
Pour conserver un maximum de lisibilité, il n'est pas possible de répondre aux publications.

----

### Le fil #Propagation réseaux sociaux

Ici, tous les groupes sont invités à partager leurs publications sur les réseaux sociaux, poursuivant un double objectif : 
- permettre la propagation et améliorer la visibilité des posts qui y sont partagés (car si chacun·e va liker ou va partager le post, nous augmentons sa portée)
- aussi offrir à chacun·e une vision globale des actualités du mouvement.

----

### Le fil #le_coin_militant

C'est l'espace pour relayer des actualités militantes, des articles intéressants qui traite de fond politique ou de stratégie et que vous souhaitez partager au réseau.

----

### Le fil #questions_et_annonces

Le fil pour poser vos questions si vous avez besoin d'être orienté vers la bonne personne, le bon fil, que vous avez besoin d'un coup de main ou que vous souhaitez passer une petite annonce au réseau !

----

### Le fil #le_coin_detente

Que dire ? Blagues, parodies, etc. ne vous prenez pas trop au sérieux !

----

## Rejoindre un fil de discussion

----

### Les fils publics

* Cliquez sur ![](media/04-rocket-chat-menu-liste-discussions.png) pour afficher la liste des fils publics
* Cliquez sur le fil que vous souhaitez rejoindre

![](media/08-rocket-chat-canal-liste.png)

----

* Vous obtenez un aperçu du fil : cliquez sur "rejoindre" pour confirmer.

![](media/08-rocket-chat-canal-rejoindre.png)

----

### Les groupes privés

* Vous y accédez par invitation par un-e propriétaire du fil ou un-e admin
* Il n'existe pas de liste des groupes privés existants

----

## Prendre connaissance de la charte

* Pour clarifier l'usage de cet outil accessible à l'ensemble du réseau, une charte d'utilisation de Rocket.Chat a été rédigée. 
* Le lien est accessible sur la page d'accueil du chat. 
* Merci de la lire attentivement !

----

## Renseigner sa clef de chiffrement E2E

----

* E2E signifie “end to end”, expression anglaise pour chiffrement “de bout en bout”. Ce protocole permet de sécuriser les communications.
* Rocket.Chat crée automatiquement un mot de passe E2E pour chaque utilisateurice. En cas d’oubli du mot de passe E2E suivre les étapes suivantes : mon compte > sécurité > chiffrement de bout en bout > réinitialiser la clé de chiffrement.

---

# Gestion des fils

----

## Créer un fil

----

-   Cliquez sur ![](media/04-rocket-chat-menu-creer-canal.png) pour ouvrir l'interface suivante :

![](media/11-rocket-chat-canal-creation.png)

<aside class="notes">

Pour créer un nouveau fil, cliquer sur le bouton d'édition dans le menu de gauche. Le créateur du fil en devient le seul propriétaire.

Il est possible de créer un fil public ou privé, et de modifier ses paramètres par la suite, donc attention à la confidentialité de vos échanges et la vie privée de vos interlocuteurs !

</aside>

----

- Fil public ou privé ? Un fil public peut être rejoint ou lu par n'importe quel membre du réseau, et apparaîtra dans la liste des fils, n'y communiquez aucune information confidentielle.
- Le mode `Lecture seule` ne permet qu'à certaines personnes d'écrire sur le fil
- Le mode `Canal de diffusion` est une variante plus souple du mode `Lecture seule` : seules les personnes autorisées peuvent écrire de nouveaux messages, mais les autres peuvent répondre
- Nom du fil : choisissez un nom clair, court, et faîtes apparaître le nom de votre groupe si besoin

----

## Modérer un fil

La personne qui crée un fil devient “propriétaire" du fil. 

Dès lors, elle peut :

-   Ajouter des modérateurs sur ce fil
-   Modifier les paramètres du fil

----

Les modérateurs (et le propriétaire) du fil peuvent :

-   Ajouter des personnes au fil
-   Exclure des personnes du fil
-   Retirer la possibilité d'écrire à certaines personnes
-   Modifier le nom du fil

----

## Modifier un fil

Cliquer sur ![](media/09-rocket-chat-canal-infos-menu.png) puis sur "Modifier" pour éditer les paramètres du fil (nom, sujet, description, annonces, accès public ou privé, lecture seule ou non).

-    La modification des paramètres est réservée aux propriétaires du fil
-    Le créateur d'un fil, propriétaire par défaut, peut désigner un⋅e ou plusieurs propriétaires (_owner_) supplémentaires

---- 

## Créer une discussion ou un fil ?

----

### Une discussion, c'est quoi ?

* En plus des fils publics et privés, il existe la possibilité de créer des "discussions", qui est une sorte de sous-fil.
* Cette fonctionnalité permet d'éviter de multiplier les fils pour un même cercle de personnes.
* Par défaut, la discussion est ouverte aux membres du fil dont elle dépend, ce qui permet une certaine transparence, sans que tout le monde voie tous les messages pratico pratique et les notifications qui vont avec.

----

### Exemple d'utilisation

Votre groupe a un fil privé "ANV-COP21 Biduleville", vous pouvez créer une discussion lorsque quelques personnes du groupe se chargent préparer la prochaine marche climat ou une réunion de coordination, des tâches qui prennent du temps et qui impliquent un échange assez intensif de messages, mais qui ne concernent que des personnes qui font déjà partie du fil de votre groupe.

----

### Créer une discussion :

* Ouvrez le fil dont elle va dépendre
* Cliquez sur le bouton + à droite de la zone de saisie 

![](media/14-rocket-chat-zone-saisie.png)

----

* Cliquez sur "Discussion"
* Complétez les informations et validez
* Une notification apparaitra dans le fil dont dépend la discussion

![](media/15-rocket-chat-notif-discussion.png)

----

### Retrouver les discussions liées à un fil

* Cliquez sur les trois points en haut à droite de la fenêtre
* Dans le menu, cliquez sur "Discussions"
* La liste des discussions s'affichent dans une barre latérale à droite de votre écran

---

# Utilisation régulière

----

## Mise en forme des messages

Vous cherchez des informations qui concernent spécifiquement la mise en forme des messages (insertion de liens, sauts de lignes, listes à puce, citations, titres...) ?

[Ce tutoriel](https://alternatiba.frama.io/rocket-chat/rocket-chat-formatage-messages.html) est à votre disposition !


----

## Mentionner des personnes

----

-   Entrez `@` puis les premières lettres du pseudo
-   Pour notifier tous les membres du fil : `@all`
-   Pour notifier les personnes connectées, : `@here`

![](media/07-rocket-chat-canal-mention-3.png)

* **Attention** `@all` ne fonctionne pas pour des fils de plus de 100 personnes

----

![](media/07-rocket-chat-canal-mention-4.png)

----

## Retrouver ses “mentions” dans une discussion

----

* Dans le menu du fil de discussion en haut à droite, cliquez sur "mentions" dans le menu

![](media/07-rocket-chat-canal-mention-liste.png)

<aside class="notes">

Vous n'avez pas consulté le chat depuis un moment et pourtant des gens vont ont interpellé dans les discussions en utilisant votre pseudo (@pseudo) ?

Retrouvez l'ensemble de ces interpellations ou “mentions” avec le bouton “@” dans le menu de droite, appelé aussi “mentions”.

En cliquant sur le symbole “petite roue crantée” à côté du pseudo de celui qui vous mentionne, vous avez une petite main qui s'affiche et vous permet d'arriver directement à l'endroit de la discussion où vous avez été mentionné.

</aside>

----

## Afficher la liste des membres d'un fil

----

* Dans le menu en haut à droite cliquer sur ![](media/09-rocket-chat-canal-membres-menu.png)

![](media/09-rocket-chat-canal-membres-liste.png)

----

## Entamer une conversation privée avec un autre membre

----

* Cliquer sur le nom du destinataire
* Cliquer sur "conversation"

![](media/12-rocket-chat-discussion-personne.png)

----

## Répondre dans un thread

----

* Les intérêts
    * Éviter que les sujets de conversation s'entremèlent
    * Distinguer au premier coup d'oeil les sujets de discussion (en gros) et les réactions (en petit)
    * Faire apparaître les échanges dans une barre latérale

(*illustration à venir*)

<aside class="notes">

![](media/13-rocket-chat-reponse-thread.png)

</aside>

----

* Pour démarrer un thread
    * Cliquez sur les trois points en haut à droite qui apparaissent au survol du message
    * Cliquez sur "reply in thread"
* Pour répondre dans un thread existant
    * Cliquez sur une des réponses précédentes pour afficher la barre latérale
    * Tapez votre texte dans la barre latérale

----

## Utiliser l'outil recherche

----

Première étape : aller dans le fil où le message a été posté. Si vous ne savez pas, allez dans n'importe quel fil et cochez la case "recherche globale" (mais la recherche sera plus longue).

----

Voilà les filtres que vous pouvez utiliser pour préciser la recherche :

- `from:me` pour rechercher vos messages.
- `from:user.name` pour rechercher les messages d'une personne donnée (entrer son pseudo @picsou et non son nom "Picsou (Picsouville)" : par exemple `from:picsou`)
- `has:url` ou `has:lien` pour les messages qui contiennent un lien donné.
- `before:dd/mm/yyyy`, `after:dd/mm/yyyy` and `on:dd/mm/yyyy` pour obtenir les messages qui ont été envoyé avant, après ou à la date considérée.

---

# Gestion des préférences

----

## Accéder aux préférences

----

Pour éditer les préférences, cliquer sur votre avatar puis "Mon compte"

![](media/10-rocket-chat-preferences.png)

----

## Éditer le nom affiché

----

* Aller dans "Mon compte"
* Cliquer sur Profil

![](media/10-rocket-chat-preferences-profil.png)

----

## Gérer les notifications

----

### Gérer l'ensemble des notifications

* Cliquez sur votre avatar puis "Mon compte"
* Cliquez sur "Préférences"
* Déroulez la page jusqu'à la section "Notifications"
* Choisissez vos réglages favoris !

----

### Gérer les notifications d'un fil

* Vous pouvez modifier les notifications d'un fil spécifique
* Cliquez sur les trois points en haut à droite de la fenêtre
* Cliquez sur "Préférences de notifications"
* Faites vos réglages !

---

# Nous contacter

----

## Aide

- Vous avez suivi le tutoriel mais ça ne fonctionne pas ?
- Vous n'avez pas trouvé l'info que vous cherchiez ?

Envoyez un message sur le fil #aide_rocketchat !

----

## Améliorations

Vous souhaitez donner votre avis sur ce tutoriel ? Cela nous intéresse ! Ecrivez-nous vos remarques sur le fil #aide_rocketchat

Merci d'avance pour votre aide !

----

Partiellement issu de [Wikibooks - Construire des communs / chat](https://fr.wikibooks.org/wiki/Construire_des_communs/Contribution/Chat)

*Simon Sarazin, Jack Potte, Numahell, Suzy,…*

<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/fr/88x31.png" /></a><br />Ce contenu est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 France</a>.
